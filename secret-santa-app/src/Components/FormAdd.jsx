import '../css/style.css';
import React from 'react'
import { useState } from 'react'

export default function FormAdd(props) {

    const [personName, setPersonName] = useState("");
    const [personEmail, setPersonEmail] = useState("");


    function addPerson(event){
        event.preventDefault();
        let person = {Name: personName, Email: personEmail};

        if(person.Name === ""){
            props.updateWarning("Invalid Name");
            return;
        }

        if(!validateEmail(person.Email)){
            props.updateWarning("Invalid Email");
            return;
        } 
        
        props.createPerson(person);
        

    }
    function deletePerson(event){
        event.preventDefault();
        props.removePerson();        

    }

    function validateEmail(email){

        let result = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
        return result;
    }



    return(
        <form className='FormAdd'>
            <div className='FormSection'>
                <label className='FormLabel'>Name: </label>
                <input className="InputBox" type='text' placeholder="John Smith" onChange={e => setPersonName(e.target.value.normalize('NFC'))}/>
            </div>
            <div className='FormSection'>
                <label className='FormLabel'>Email: </label>
                <input className="InputBox" type='email' placeholder="JSmith@email.com"onChange={e => setPersonEmail(e.target.value.normalize('NFC')) }/>
            </div>
            
            <div className='buttonSection'>
                <button className='AddButton' onClick={event => addPerson(event)}>+</button>
                <button className='removeButton' onClick={event => deletePerson(event)}>-</button>
            </div>
            

        </form>


    )
}