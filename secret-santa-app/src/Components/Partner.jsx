import '../css/style.css';
import React from 'react'
import { useState } from 'react'


export default function Partner(props) {

    const [isVisible, setVisible] = useState(false);

    function ChangeVisiblity(event){
        event.preventDefault();

        let newVisibility = !isVisible;
        setVisible(newVisibility);
    }


    return(
        <div className={ (isVisible ? "Partner" : "PartnerInvisible")} onClick={ChangeVisiblity}>
            <div className={ (isVisible ? "PartnerInfo" : "PartnerInfoInvisible")} >
                <p>{props.Name}</p>
            </div>

        </div>


    )
}