import '../css/style.css';
import React from 'react'
import FormAdd from './FormAdd';
import Person from './Person';
import { useState} from 'react'


export default function FormBody(props) {
    
    const [nameList, setNameList] = useState([])

    let people;

    function createPerson(PersonData){
        setNameList(nameList => [PersonData, ...nameList])
        props.updateShowWarning(false);
    }
    function removePerson(){
        let tempList = nameList.slice();
        tempList.shift()
        if(tempList.length < 1){
            setNameList([]);
        } else {
            setNameList(tempList);
        }

        props.updateShowWarning(false);
    }

    

    function randomize(event){
        event.preventDefault();

        let assignedPartner = [];
        let partner = [];

        if(nameList.length < 3) {
            console.log("Not enough people");
            props.updateWarning("Please Add More People");
            return;
        } else {

        partner = assignPartner();
        
        for(let j =0; j < partner.length; j++){
            assignedPartner.push({Person:nameList[j], Partner: nameList[partner[j]]});
        }
        
        props.displayResults(assignedPartner);
        }
    }

    function generatePartner(){
        let randomNumber = Math.floor(Math.random() * nameList.length);
        return randomNumber;
    }


    function assignPartner(){
        let partner = [];
        let randomNumber = 0;

        for(let i = 0; i < nameList.length; i++){
            randomNumber = generatePartner();

            while(i === randomNumber || partner.includes(randomNumber)){
                randomNumber = generatePartner();

                if(i === nameList.length -1 && randomNumber === i) {
                    partner = [];
                    i= 0;
                }
            }
            partner.push(randomNumber);
        }
        return partner;
    }

    function updateList(){
        people = nameList.map((person,index) => <Person key={index} Name={person.Name} Email={person.Email}/>);
        return people;
    }



    return(
        <div className='Form'>
            <h1>
                Please enter participant information (minimum 3):
            </h1>
          
            <div className='FormArea'>
                <div>
                    <FormAdd updateWarning={props.updateWarning} removePerson={removePerson} createPerson={createPerson}/>
                    <button className='FinalizeButton' onClick={randomize}>start</button>
                </div>
                
                <div className='PeopleContainer'>
                    
                    {updateList()}
                    
                </div>
                
            </div>
            
            
        </div>
        

    )
}