//Emailjs Code from https://betterprogramming.pub/how-to-use-emailjs-for-automated-emails-react-js-d0828165e2f

import '../css/style.css';
import React from 'react'
import Person from './Person';
import Partner from './Partner';
import Warning from './Warning';
import { useState } from 'react';


export default function Result(props) {

    // eslint-disable-next-line
    let partners;

    // eslint-disable-next-line
    const [emailSuccessful, setEmailSuccessful] = useState(false);
    // eslint-disable-next-line
    const [hasProblems, setProblems] = useState(false);

    function setSuccessMessage(){
        setEmailSuccessful(true);
    }

    // eslint-disable-next-line
    function setErrors(){
        hasProblems(true);
    }

    


    function handleSubmit(event){
        event.preventDefault();
        console.log("submitting...");
        const templateId = "template_cr9gzc9"
        
        for (let i = 0; i < props.partnerList.length; i++){

            sendEmail(templateId, {
                person_name: props.partnerList[i].Person.Name,
                person_email: props.partnerList[i].Person.Email,
                partner_name: props.partnerList[i].Partner.Name,
            })

        }
        if(!hasProblems){
            setSuccessMessage();
        }
        
    }

    
    // function tempsendEmail (templateID, variables){
    //     console.log("success!");
    // }



    function sendEmail (templateID, variables){


        window.emailjs.send(
            'service_m30wR4t', templateID, variables, "yLRCE1UW0hwN0-3ip"
        ).then(res => {
            console.log("success");
        })
        .catch(err => hasProblems())
    }


    return(
        <div className='ResultArea'>
            <h1> Pairing Successful!</h1>
            <p> Click on a person from the right side column to reveal their name!</p>
            {(hasProblems) ? <div className='ResultWarning'><Warning WarningType={'Warning'} WarningMessage="An error has occured while sending emails"/> </div>: <br/>}
            {(!hasProblems && emailSuccessful) ? <div className='ResultWarning'><Warning WarningType={'Success'} WarningMessage="Success!"/> </div>: <br/>}
            <div className='PeopleContainer'>
                    
                    {partners = props.partnerList.map((group,index) =>
                    <div className='ResultBox' key={index}>
                     <Person  Name={group.Person.Name} Email={group.Person.Email}/>  
                     <Partner  Name={group.Partner.Name} Email={group.Partner.Email}/>  
                     </div>)}
                     <button className='EmailButton' onClick={handleSubmit} >Send Email</button>
            </div>

        </div>


    )
}