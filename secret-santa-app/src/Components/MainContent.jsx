import '../css/style.css';
import React from 'react'

export default function MainContent(props) {


    return(
        <div className='Info'>
            <p> Welcome to the Secret Santa Matchmaker!</p>
            <p> This website will randomly assign everyone their Secret Santa!</p>
            <button className='StartButton' onClick={props.startAppEvent}>Start</button>
        </div>


    )
}