// import logo from './logo.svg';
import React from 'react'
import './App.css';
import TitleHeader from './Components/Header';
import MainContent from './Components/MainContent';
import FormBody from './Components/Form';
import Result from './Components/Results';
import Warning from './Components/Warning';
import { useState } from 'react'



function App() {

  const [started, setStarted] = useState(false);

  const [showResults, setShowResults] = useState(false);

  const [showWarning, setShowWarning] = useState(false);

  const [warningMessage, setWarningMessage] = useState("");

  const [finalizedPeople, setFinalizedPeople] = useState([]);

  function startApp() {
    setStarted(true);
  }

  function displayResults(newPartner){
    setShowResults(true);
    updatePeople(newPartner);
    setShowWarning(false);
  }

  function updatePeople(newPartner){
    setFinalizedPeople(newPartner);
  }

  function updateWarning(warning){
    updateShowWarning(true);
    setWarningMessage(warning);
  }

  function updateShowWarning(status){
    setShowWarning(status);
  }



  return (



    <div className="App-header">
        <TitleHeader/>
        {(started && showWarning) ? <Warning WarningType={'Warning'} WarningMessage= {warningMessage}/> : <br/>}
        <div className='MiddleContent'>
          {(!started) ? <MainContent startAppEvent = {startApp}/> : <FormBody updateShowWarning={updateShowWarning} updateWarning={updateWarning} displayResults = {displayResults}/>}
          {(started && showResults) ? <Result partnerList = {finalizedPeople}/> : <br/>}

        </div>
    </div>
  );
}

export default App;
